FROM apache/airflow:2.7.0
COPY requirements.txt /tmp
RUN pip install --upgrade pip
RUN python3 -m pip install -r /tmp/requirements.txt

USER root
RUN apt-get update && apt-get install -y wget
USER 0