import os
import bs4
import requests
from PIL import Image
import io
import unicodedata

import time

from lib.PDF import PDF


class HakoLNPacker:
    def __init__(self, init_url, output_path):
        self.init_url = init_url
        self.output_path = output_path
        self.pdf = PDF()
        self.target_volume = None
        self.image_queue = []
    
    def __request_and_parse_soup__(self, url):
        response = requests.get(url,headers={"Accept-Language":"en-US"})
        if str(response.status_code) == "200":
            return bs4.BeautifulSoup(response.text,"html.parser")
        else:
            return None
        
    def __set_target_volume__(self, soup):
        self.target_volume = self.__get_volume__(soup)

    def __get_volume__(self, soup):
        titleItem = soup.findAll('h2', class_='title-item')
        if titleItem and len(titleItem) > 0 and titleItem[0].text:
            title = titleItem[0].text.split(' ')
            return float(title[1]) if len(title) > 1 else None
        return None

    def __pack_pdf__(self):
        print("Creating Vol " + str(self.target_volume))
        self.pdf.output(self.output_path + '/Vol ' + str(self.target_volume) + '.pdf','F')
        
    def download(self):
        url = self.init_url
        while(True):
            soup = self.__request_and_parse_soup__(url)
            if soup:
                vol = self.__get_volume__(soup)
                if self.target_volume is None:
                    print("Begin processing Volume " + str(vol))
                    self.__set_target_volume__(soup)
                if vol == self.target_volume:
                    self.__process_chapter__(soup)
                    next_chapter = soup.find(class_='rd_sd-button_item rd_top-right')
                    if next_chapter:
                        url = "https://docln.net" + next_chapter['href']
                        time.sleep(5)
                    else:
                        break
                else:
                    break
        self.__pack_pdf__()

    def __process_chapter__(self, soup):
        title = self.__process_title__(soup)
        print("Processing Chapter " + title)

        contents = soup.find(id='chapter-content')
        for content in contents:
            self.__process_content__(content)

        if len(self.image_queue) > 0:
            self.__add_image__(self.image_queue)
            self.image_queue = []
    
    def __process_title__(self, soup):
        title = soup.findAll('h4', class_='title-item')[0].text
        if title != "Minh họa":
            self.pdf.add_title(title)
        return title

    def __process_content__(self, content):
        if type(content) is not bs4.NavigableString and content.name == 'p':
            if content.img != None:
                self.__save_image_content__(content)
            else:
                text = content.text.encode('latin-1', 'replace').decode('utf8')
                if len(self.image_queue) > 0 and not self.pdf.is_in_new_page(text):
                    self.__add_image__(self.image_queue)
                    self.image_queue = []
                self.pdf.add_text(text)

    def __save_image_content__(self, content):
        image_name = content.img['src'].split("/")[-1]
        image_path = os.path.dirname(os.path.realpath(__file__)) + "/Images/" + image_name
        image_url = content.img['src']
        if self.__save_image__(image_path, image_url):
            self.image_queue.append(image_path)
        time.sleep(3)

    def __add_image__(self, images):
        for image in images:
            self.pdf.add_image(image)

    def __save_image__(self, image_path, image_url):
        response = requests.get(image_url, stream=True)
        if str(response.status_code) == '200':
            image = Image.open(io.BytesIO(response.content))
            image = image.convert('RGBA')
            image.load()
            converted_image = Image.new('RGBA', image.size, (255, 255, 255))
            image = Image.alpha_composite(converted_image, image).convert("RGB")
            width, height = image.size
            if width > height:
                image = image.transpose(Image.ROTATE_90)
            image.save(image_path)
            return True
        else:
            print("Failed to save image - " + str(image_url) + " - " + str(response.status_code))
        return False