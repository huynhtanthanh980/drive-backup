from fpdf import FPDF
import os
import math

class PDF(FPDF):
    def __init__(self):
        FPDF.__init__(self, orientation='P', unit='mm', format='A4')
        self.add_font('calibri','',os.path.dirname(os.path.realpath(__file__)) + "tools/Fonts/DejaVuSans-Bold.ttf",uni=True)
        self.add_font('calibri','B',os.path.dirname(os.path.realpath(__file__)) + "tools/Fonts/DejaVuSansCondensed.ttf",uni=True)
        self.set_font('calibri','',18)
        self.set_margins(0, 0, 0)
        self.set_xy(0,0)
        self.add_page()
        self.cellHeight = 12
        self.cellWidth = 170
        self.does_footer_needed = True

    def footer(self):
        if self.does_footer_needed:
            self.set_y(-10)
            self.set_font('calibri','',15)
            self.cell(0, 10, '%s' % self.page_no(), 0, 0, 'C')

    def is_in_new_page(self, content):
        if self.get_y() <= 20:
            return False
        length = self.get_string_width(content)
        rows = math.floor(length / self.cellWidth)
        rowHeight = rows * self.cellHeight
        return 272 - self.get_y() > rowHeight

    def add_title(self, title):
        if self.get_y() > 20:
            self.add_page()
        self.set_margins(20, 20, 20)
        self.set_xy(20,20)
        self.set_font('calibri','B',25)
        self.multi_cell(170, 5, txt ='', align = 'L')
        self.multi_cell(170, 15, txt ="   " + title, align = 'L')
        self.multi_cell(170, 20, txt ='', align = 'L')
        self.set_font('calibri','',18)
        self.set_title(title)

    def add_image(self, image):
        self.set_margins(0, 0, 0)
        self.set_xy(0,0)
        self.image(image,0,0,210,297)
        self.does_footer_needed = False
        self.add_page()
        self.reset_page()
    
    def add_text(self, content):
        self.multi_cell(self.cellWidth, self.cellHeight, txt = content, align = 'L')
    
    def reset_page(self):
        self.set_margins(20, 20, 20)
        self.set_xy(20,20)
        self.set_font('calibri','',18)
        self.does_footer_needed = True
    
    def new_page(self):
        self.add_page()