from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator, BranchPythonOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.exceptions import AirflowFailException
from airflow.utils.task_group import TaskGroup

from datetime import datetime, timedelta
from pytz import timezone
import json

def prepare_available_movies(all_movies_dir, temp_compressed_movies_dir, uploaded_movies):
    import random
    import os

    all_movies = os.listdir(all_movies_dir)
    temp_compressed_movies = os.listdir(temp_compressed_movies_dir)

    available_movies = list(set(all_movies) - set(uploaded_movies))
    random.shuffle(available_movies)

    if len(temp_compressed_movies) > 0:
        for movie in temp_compressed_movies:
            if movie in available_movies:
                available_movies.remove(movie)
            available_movies.insert(0, movie)

    return available_movies

def select_and_create_upload_target(**kwargs):
    from uuid import uuid4
    from OneDriveBackup.PostgreUtil import PostgreUtil

    postgreUtil = PostgreUtil()

    pre_select_movies = kwargs["params"]["Movie"]
    is_updated = kwargs["params"]["IsUpdated"]
    root_dir = kwargs["params"]["RootDir"]
    category = kwargs["params"]["Category"]
    temp_dir = kwargs["params"]["TempDir"]

    all_movies_dir = root_dir + "/" + category
    latest_record_movie_names = sorted([record.name for record in postgreUtil.get_latest_record()])

    if pre_select_movies:
        if not is_updated and pre_select_movies in latest_record_movie_names:
            raise AirflowFailException('Selected movie has no update')
        else:
            available_movies = [pre_select_movies]
                
    else:
        available_movies = prepare_available_movies(all_movies_dir, temp_dir, latest_record_movie_names)

    select_upload_movie_name = None
    movie_uuid = str(uuid4())
    upload_to_remote = ""

    while not select_upload_movie_name and len(available_movies) > 0:
        try:
            new_movie = available_movies[0]
            upload_to_remote = postgreUtil.get_last_upload_remote(new_movie)
            postgreUtil.create_upload_record(movie_uuid, new_movie, category, root_dir + "/" + category + "/" + new_movie, upload_to_remote)
            select_upload_movie_name = new_movie
        except Exception as error:
            print("Create upload record failed: " + str(error))
            available_movies.remove(new_movie)

    if not select_upload_movie_name:
        raise AirflowFailException('No target is available')
    else:
        print("\tChoosen Movie ID: " + movie_uuid)
        print("\tChoosen Movie Name: " + select_upload_movie_name)
        print("\tUpload to Remote: " + (upload_to_remote if upload_to_remote else "Unassigned"))
        ti = kwargs['ti']
        ti.xcom_push(key="movie_id", value=movie_uuid)
        ti.xcom_push(key="movie_name", value=select_upload_movie_name)

def check_folder_and_compress(**kwargs):
    import os

    root_dir = kwargs["params"]["RootDir"]
    category = kwargs["params"]["Category"]
    temp_dir = kwargs["params"]["TempDir"]
    movie_dir = kwargs['ti'].xcom_pull(key="movie_name", task_ids="Selecting.select_and_create_upload_target")

    movieDir = root_dir + "/" + category + "/" + movie_dir
    compressedCheckDir = temp_dir + "/" + movie_dir + "/done.txt"

    if os.path.exists(movieDir):
        if os.path.exists(compressedCheckDir):
            return "Uploading.update_uploading_state"
        else:
            return "Preparing.delete_checksum_and_temp"
    else:
        raise AirflowFailException('Directory is not existed')
    
def hash_movie_directory(**kwargs):
    from OneDriveBackup.HashUtil import HashUtil

    root_dir = kwargs["params"]["RootDir"]
    category = kwargs["params"]["Category"]
    movie_dir = kwargs['ti'].xcom_pull(key="movie_name", task_ids="Selecting.select_and_create_upload_target")
    movieDir = root_dir + "/" + category + "/" + movie_dir
    HashUtil().hash_folder(movieDir)
    
def divide_compress(**kwargs):
    import os

    dir = kwargs["params"]["TempDir"]
    movie_dir = kwargs['ti'].xcom_pull(key="movie_name", task_ids="Selecting.select_and_create_upload_target")
    os.chdir(dir + "/" + movie_dir)
    file_list = os.listdir()

    for i in range(0, len(file_list)):
        file = file_list[i]
        os.renames(dir + "/" + movie_dir + "/" + file, dir + "/" + movie_dir + "/path " + str((i % 4) + 1) + "/" + file)

    for i in range(1, configs["divide_count"] + 1):
        try:
            os.mkdir(dir + "/" + movie_dir + "/path " + str(i), 0o666)
        except:
            pass

def choose_remote(**kwargs):
    import re
    from OneDriveBackup.PostgreUtil import PostgreUtil

    ti = kwargs['ti']

    movie_upload_id = ti.xcom_pull(key="movie_id", task_ids="Selecting.select_and_create_upload_target")
    db_remote = PostgreUtil().get_remote(movie_upload_id)

    if not db_remote:
        min_size = None
        min_remote = None

        for remote in configs["remotes"]:
            bash_output = ti.xcom_pull(task_ids='Selecting.check_remote_size_' + remote)
            size = int(re.findall("\([^ ]*", bash_output)[0].replace("(", ""))
            if min_size is None or min_size > size:
                min_size = size
                min_remote = remote
        
        return min_remote
    else:
        return db_remote

with open("dags/OnedriveBackup/config.json") as json_config:
    configs = json.load(json_config)
    config_params = configs["params"]

with DAG(
    dag_id='OnedriveBackup',
    default_args={
        'owner': 'airflow',
        'depends_on_past': False,
        'start_date': datetime(2023, 6, 7, tzinfo=timezone('Asia/Ho_Chi_Minh')),
        'retries': configs["retries"],
        'retry_delay': timedelta(seconds=1),
    },
    description='A simple DAG',
    schedule_interval=configs["schedule_interval"],
    params={
        "Category": config_params["Category"],
        "RootDir": config_params['RootDir'],
        "TempDir": config_params['TempDir'],
        "Movie":"",
        "IsUpdated": False
    },
    catchup=False,
    max_active_runs=configs["max_active_runs"]
) as dag:
    with TaskGroup(group_id="Selecting") as Selecting:
        select_and_create_upload_target_dag = PythonOperator(
            task_id='select_and_create_upload_target',
            python_callable=select_and_create_upload_target,
            dag=dag,
        )

        check_remote_size = [
            BashOperator(
                task_id='check_remote_size_' + remote,
                bash_command="cd /opt/airflow/tools/rclone; ./rclone size " + remote + ":",
                dag = dag
            ) for remote in configs["remotes"]
        ]

        choose_remote_dag = PythonOperator(
            task_id='choose_remote',
            python_callable=choose_remote,
            dag=dag,
        )

        select_and_create_upload_target_dag >> check_remote_size >> choose_remote_dag

    check_folder_and_compress_dag = BranchPythonOperator(
        task_id='check_folder_and_compress',
        python_callable=check_folder_and_compress,
        dag=dag,
    )

    with TaskGroup(group_id="Preparing") as Preparing:
        delete_checksum_and_temp = BashOperator(
            task_id='delete_checksum_and_temp',
            bash_command="""
                rm \"{{ params.RootDir }}/{{ params.Category }}/{{ ti.xcom_pull(key='movie_name', task_ids='Selecting.select_and_create_upload_target') }}/checksum.md5\";
                rm \"{{ params.RootDir }}/{{ params.Category }}/{{ ti.xcom_pull(key='movie_name', task_ids='Selecting.select_and_create_upload_target') }}/checksum.sha1\";
                rm \"{{ params.RootDir }}/{{ params.Category }}/{{ ti.xcom_pull(key='movie_name', task_ids='Selecting.select_and_create_upload_target') }}/checksum.sha256\";
                rm -rf \"{{params.TempDir}}/{{ ti.xcom_pull(key='movie_name', task_ids='Selecting.select_and_create_upload_target') }}\"
            """,
            dag = dag
        )

        update_hashing_state = PostgresOperator(
            task_id="update_hashing_state",
            postgres_conn_id="onedrive-postgresql",
            sql="""
                UPDATE "Movies".upload_record SET update_time = now(), status = 'HASHING' where id = '{{ ti.xcom_pull(key='movie_id', task_ids='Selecting.select_and_create_upload_target') }}';
                """,
            dag=dag
        )

        hash_movie_directory_dag = PythonOperator(
            task_id='hash_movie_directory',
            python_callable=hash_movie_directory,
            dag=dag,
        )

        update_compressing_state = PostgresOperator(
            task_id="update_compressing_state",
            postgres_conn_id="onedrive-postgresql",
            sql="""
                UPDATE "Movies".upload_record SET update_time = now(), status = 'COMPRESSING' where id = '{{ ti.xcom_pull(key='movie_id', task_ids='Selecting.select_and_create_upload_target') }}';
                """,
            dag=dag
        )

        compress = BashOperator(
            task_id='compress',
            bash_command="""
                cd /opt/airflow/tools/7z;
                ./7zz a -mmt -mx0 -v2048m -t7z -p123456 \"{{ params.TempDir }}/{{ ti.xcom_pull(key='movie_name', task_ids='Selecting.select_and_create_upload_target') }}/{{ ti.xcom_pull(key='movie_name', task_ids='Selecting.select_and_create_upload_target') }}\" \"{{ params.RootDir }}/{{ params.Category }}/{{ ti.xcom_pull(key='movie_name', task_ids='Selecting.select_and_create_upload_target') }}\";
            """,
            dag = dag
        )

        divide_compress_dag = PythonOperator(
            task_id='divide_compress',
            python_callable=divide_compress,
            dag=dag,
        )

        add_done_file = BashOperator(
            task_id='add_done_file',
            bash_command="echo '{{ ti.xcom_pull(key='movie_id', task_ids='Selecting.select_and_create_upload_target') }}' > \"{{ params.TempDir }}/{{ ti.xcom_pull(key='movie_name', task_ids='Selecting.select_and_create_upload_target') }}/done.txt\";",
            dag = dag,
            trigger_rule='none_failed'
        )

        delete_checksum_and_temp >> update_hashing_state >> hash_movie_directory_dag >> update_compressing_state >> compress >> divide_compress_dag >> add_done_file

    with TaskGroup(group_id="Uploading") as Uploading:
        update_uploading_state = PostgresOperator(
            task_id="update_uploading_state",
            postgres_conn_id="onedrive-postgresql",
            sql="""
                UPDATE "Movies".upload_record SET remote = '{{ ti.xcom_pull(task_ids='Selecting.choose_remote') }}', update_time = now(), status = 'UPLOADING' where id = '{{ ti.xcom_pull(key='movie_id', task_ids='Selecting.select_and_create_upload_target') }}';
                """,
            dag=dag
        )

        upload_path = [
            BashOperator(
                task_id='upload_path_' + str(i),
                bash_command="cd /opt/airflow/tools/rclone; ./rclone copy -v \"{{ params.TempDir }}/{{ ti.xcom_pull(key='movie_name', task_ids='Selecting.select_and_create_upload_target') }}/path " + str(i) + "\" \"{{ ti.xcom_pull(task_ids='Selecting.choose_remote') }}:{{ params.RootDir }}/{{ params.Category }}/{{ ti.xcom_pull(key='movie_name', task_ids='Selecting.select_and_create_upload_target') }}\"",
                dag = dag
            ) for i in range(1, configs["divide_count"] + 1)
        ]

        update_uploading_state >> upload_path

    with TaskGroup(group_id="Cleaning") as Cleaning:
        delete_remote_if_failed = BashOperator(
            task_id='delete_remote_if_failed',
            bash_command="cd /opt/airflow/tools/rclone; ./rclone purge -v \"{{ ti.xcom_pull(task_ids='Selecting.choose_remote') }}:{{ params.RootDir }}/{{ params.Category }}/{{ ti.xcom_pull(key='movie_name', task_ids='Selecting.select_and_create_upload_target') }}\"",
            dag = dag,
            trigger_rule='one_failed'
        )

        delete_temp = BashOperator(
            task_id='delete_temp',
            bash_command="rm -rf \"{{params.TempDir}}/{{ ti.xcom_pull(key='movie_name', task_ids='Selecting.select_and_create_upload_target') }}\"",
            dag = dag
        )

        [delete_remote_if_failed, delete_temp]

    update_record = [
        PostgresOperator(
            task_id="delete_upload_record",
            postgres_conn_id="onedrive-postgresql",
            sql="""DELETE from "Movies".upload_lock where id = '{{ ti.xcom_pull(key='movie_id', task_ids='Selecting.select_and_create_upload_target') }}';""",
            dag=dag,
            trigger_rule='all_done'
        ),
        PostgresOperator(
            task_id="set_failed_task",
            postgres_conn_id="onedrive-postgresql",
            sql="""UPDATE \"Movies\".upload_record  SET update_time = now(), status = 'FAILED' where id = '{{ ti.xcom_pull(key='movie_id', task_ids='Selecting.select_and_create_upload_target') }}';""",
            dag=dag,
            trigger_rule='one_failed'
        ),
        PostgresOperator(
            task_id="add_onedrive_record",
            postgres_conn_id="onedrive-postgresql",
            sql="""UPDATE \"Movies\".upload_record  SET update_time = now(), status = 'DONE' where id = '{{ ti.xcom_pull(key='movie_id', task_ids='Selecting.select_and_create_upload_target') }}';""",
            dag=dag,
            trigger_rule='none_failed'
        ),
    ]

    Selecting >> check_folder_and_compress_dag >> Preparing >> Uploading >> Cleaning >> update_record

if __name__ == "__main__":
    dag.cli()
