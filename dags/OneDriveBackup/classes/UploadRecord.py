class UploadRecord:
    def __init__(self, args):
        self.id = args[0]
        self.name = args[1]
        self.category = args[2]
        self.directory = args[3]
        self.remote = args[4]
        self.create_time = args[5]
        self.update_time = args[6]