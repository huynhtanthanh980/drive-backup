import hashlib
import os

class HashUtil:
    def generate_hash(self, source_name, chunk_size):
        hash_md5_func = hashlib.md5()
        hash_sha1_func = hashlib.sha1()
        hash_sha256_func = hashlib.sha256()

        print("Hashing " + str(source_name))

        with open(source_name, "rb") as f:
            chunk = f.read(chunk_size)
            while chunk:
                hash_md5_func.update(chunk)
                hash_sha1_func.update(chunk)
                hash_sha256_func.update(chunk)
                chunk = f.read(chunk_size)
                
        md5_hash_value = hash_md5_func.hexdigest()
        sha1_hash_value = hash_sha1_func.hexdigest()
        sha256_hash_value = hash_sha256_func.hexdigest()

        print("MD5 hash value: " + md5_hash_value)
        print("SHA1 hash value: " + sha1_hash_value)
        print("SHA256 hash value: " + sha256_hash_value)

        return (md5_hash_value, sha1_hash_value, sha256_hash_value)

    def generate_dir_hash(self, source_name, chunk_size=4096):
        hash_dict = {
            "md5": {},
            "sha1": {},
            "sha256": {}
        }

        for path, dirs, files in os.walk(source_name):
            for file_name in files:
                file_dir = os.path.join(source_name, path, file_name)
                if path.replace(source_name, "") == "":
                    short_file_dir = file_name
                else:
                    short_file_dir = path.replace(source_name + "/", "") + "/" + file_name
                md5_hash_value, sha1_hash_value, sha256_hash_value = self.generate_hash(file_dir, chunk_size)
                hash_dict["md5"][short_file_dir] = md5_hash_value
                hash_dict["sha1"][short_file_dir] = sha1_hash_value
                hash_dict["sha256"][short_file_dir] = sha256_hash_value

        return hash_dict

    def hash_folder(self, source_name):
        hash_result = self.generate_dir_hash(source_name)
        for hash_name, dir_hash_dict in hash_result.items():
            with open(os.path.join(source_name, "checksum." + hash_name), "w") as f:
                for short_file_dir, hash_value in dir_hash_dict.items():
                    print(short_file_dir, hash_value)
                    f.write("{} *{}\n".format(hash_value, short_file_dir))