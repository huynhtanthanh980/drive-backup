from airflow.hooks.postgres_hook import PostgresHook
from OneDriveBackup.classes.UploadRecord import UploadRecord

class PostgreUtil:
    def __init__(self):
        self.hook = PostgresHook(postgres_conn_id='onedrive-postgresql')

    def get_latest_record(self):
        conn = self.hook.get_conn()
        cursor = conn.cursor()
        cursor.execute("""
            SELECT *
            FROM (
                SELECT DISTINCT ON (name)
                    *
                FROM   "Movies".upload_record
                ORDER  BY name, update_time DESC
            ) latest_status
            WHERE status = 'DONE';
        """)
        return [UploadRecord(row) for row in cursor.fetchall()]

    def create_upload_record(self, id, name, category, directory, remote):
        self.hook.run("""
            BEGIN TRANSACTION;
            INSERT INTO "Movies".upload_record(id, name, category, directory, remote, create_time, update_time, status) VALUES ('{id}', '{name}', '{category}', '{directory}', '{remote}', now(), now(), 'CREATED');
            INSERT INTO "Movies".upload_lock(id, name) VALUES ('{id}', '{name}');
            COMMIT;
        """.format(id = id, name = name, category = category, directory = directory, remote = remote))

    def get_last_upload_remote(self, movie_name):
        conn = self.hook.get_conn()
        cursor = conn.cursor()
        cursor.execute("""SELECT * FROM "Movies"."upload_record" where remote != '' and name = '{}' ORDER BY update_time DESC;""".format(movie_name))
        query_results = [UploadRecord(row) for row in cursor.fetchall()]
        if len(query_results) > 0:
            remote = query_results[0].remote
            return remote if remote else ""
        return ""
    
    def get_remote(self, movie_id):
        conn = self.hook.get_conn()
        cursor = conn.cursor()
        cursor.execute("""SELECT * FROM "Movies"."upload_record" where id = '{}';""".format(movie_id))
        query_results = [UploadRecord(row) for row in cursor.fetchall()]
        if len(query_results) > 0:
            remote = query_results[0].remote
            return remote if remote else ""
        return ""