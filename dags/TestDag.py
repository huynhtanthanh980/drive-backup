from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.hooks.postgres_hook import PostgresHook

from datetime import datetime
from pytz import timezone
import os
import re
import json

local_tz = timezone('Asia/Ho_Chi_Minh')

remotes = [
    "KMMDataOne",
    "KMMDataTwo",
    "KMMDataThree",
    "KMMDataFour",
    "KMMDataFive"
]

# def process_data(**kwargs):
#     rootDir = kwargs["params"]["RootDir"]
#     category = kwargs["params"]["Category"]
#     tempDir = kwargs["params"]["TempDir"]
#     print(rootDir, category, tempDir)

# with open("OnedriveBackup/config.json") as json_config:
#     configs = json.load(json_config)
#     config_params = configs["params"]

# with DAG(
#     dag_id='TestDag',
#     default_args={
#         'owner': 'airflow',
#         'depends_on_past': False,
#         'start_date': datetime(2023, 6, 7, tzinfo=timezone('Asia/Ho_Chi_Minh')),
#         'retries': configs["retries"],
#         'retry_delay': timedelta(seconds=1),
#     },
#     description='A simple DAG',
#     schedule_interval=configs["schedule_interval"],
#     params={
#         "Category": config_params["Category"],
#         "RootDir": config_params['RootDir'],
#         "TempDir": config_params['TempDir'],
#         "Movie":"",
#         "IsUpdated": False
#     },
#     catchup=False
# ) as dag:
#     process_data = PythonOperator(
#         task_id='process_data',
#         python_callable=process_data,
#         dag=dag,
#     )

#     process_data

# get_all_uploaded = [
#     BashOperator(
#         task_id='get_all_uploaded_' + remote,
#         bash_command="cd /opt/airflow/tools/rclone; ./rclone lsd --max-depth 2 '" + remote + ":Movies/TV Shows' | tr '\n' '||';",
#         dag = dag
#     ) for remote in remotes
# ]

# add_data = PostgresOperator(
#     task_id="add_data",
#     postgres_conn_id="onedrive-postgresql",
#     sql="""
#         select name from "Movies".upload_record where id = '00398c37-cb00-42d5-b64c-b22e4d085cd4';
#         """,
#     dag=dag
# )

# compress = BashOperator(
#     task_id='compress',
#     bash_command="""
#         echo {{ ti.xcom_pull(task_ids='add_data') }};
#     """,
#     dag = dag
# )

# add_data >> compress

if __name__ == "__main__":
    dag.cli()
